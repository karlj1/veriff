### Overview
The SDK module contains the following features:
1. Extract text from an ID
2. Take a photo of a single person

### Prerequisites
1. `CAMERA` permission will be requested and will be needed by the SDK once it starts its flow.

### Starting the Extract Text from ID Flow
Call the following API from any activity or fragment:
``` kotlin
MySdk.getExtractIdTextIntent(context)
startActivityForResult(intent, REQUEST_CODE)
``` 

Users will be notified via a Toast message in case any error happens within the flow.

If there are no texts detected, the SDK will prompt the user to take another photo.

If the user does not give the `CAMERA` permission, the flow will get cancelled and inform the app through the `resultCode`.

### Getting the Extracted Text from ID Flow Results
Call the `MySdk.getExtractIdTextResult()` API once you have received the `ActivityResult` from the SDK. This API will return the extracted text.
``` kotlin
private val extractIdTextLauncher =
    registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            txt_result.text = MySdk.getExtractIdTextResult(result.data!!)
        } else if (result.resultCode == MySdk.RESULT_PERMISSIONS_DENIED) {
            Toast.makeText(
                this,
                "Permissions not granted",
                Toast.LENGTH_LONG
            ).show()
        }
    }
```

### Starting the Take a Photo of One Person Flow
Call the following API from any activity or fragment:
``` kotlin
MySdk.getCapturePersonPhotoIntent(this)
startActivityForResult(intent, REQUEST_CODE)
``` 

Users will be notified via a Toast message in case any error happens within the flow.

If there is not exactly one person in the photo, the SDK will prompt the user to take another photo.

If the user does not give the `CAMERA` permission, the flow will get cancelled and inform the app through the `resultCode`.

### Getting Take a Photo of One Person Flow Results
Call the `MySdk.getCapturePersonPhotoResult()` API once you have received the `ActivityResult` from the SDK. This API will return the URI of the photo.
``` kotlin
private val capturePersonPhotoLauncher =
    registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            Picasso.get()
                .load(MySdk.getCapturePersonPhotoResult(result.data!!))
                .into(image_face)
        } else if (result.resultCode == MySdk.RESULT_PERMISSIONS_DENIED) {
            Toast.makeText(
                this,
                "Permissions not granted",
                Toast.LENGTH_LONG
            ).show()
        }
    }
```