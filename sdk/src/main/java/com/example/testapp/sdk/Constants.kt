package com.example.testapp.sdk

internal object Constants {
    const val EXTRA_EXTRACTED_TEXT = "extracted_text"
    const val FILE_NAME = "sdk_current_photo"
    const val RESULT_PERMISSIONS_DENIED = 100
}