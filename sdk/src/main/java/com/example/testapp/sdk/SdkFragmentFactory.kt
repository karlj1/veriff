package com.example.testapp.sdk

import androidx.camera.core.ImageCapture
import androidx.camera.core.Preview
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import java.util.concurrent.Executors

internal class SdkFragmentFactory : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        return when (loadFragmentClass(classLoader, className)) {
            CameraFragment::class.java -> CameraFragment(
                Executors.newSingleThreadExecutor(),
                ImageCapture.Builder().build(),
                Preview.Builder().build()
            )
            else -> super.instantiate(classLoader, className)
        }
    }
}