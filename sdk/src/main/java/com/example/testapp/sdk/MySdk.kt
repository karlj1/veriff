package com.example.testapp.sdk

import android.content.Context
import android.content.Intent
import android.net.Uri

object MySdk {
    fun getExtractIdTextIntent(context: Context) =
        Intent(context, ExtractIdTextActivity::class.java)

    fun getExtractIdTextResult(intent: Intent) =
        intent.getStringExtra(Constants.EXTRA_EXTRACTED_TEXT)

    fun getCapturePersonPhotoIntent(context: Context) =
        Intent(context, CapturePersonPhotoActivity::class.java)

    fun getCapturePersonPhotoResult(intent: Intent) =
        intent.getParcelableExtra<Uri>(CameraActivity.EXTRA_IMAGE_URI)

    const val RESULT_PERMISSIONS_DENIED = Constants.RESULT_PERMISSIONS_DENIED
}