package com.example.testapp.sdk

import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

internal abstract class CameraActivity : AppCompatActivity() {

    private val cameraViewModel: CameraViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = SdkFragmentFactory()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sdk_camera)

        lifecycleScope.launch {
            cameraViewModel.uri.collect {
                it?.let {
                    onImageUriReceived(it)
                }
            }
        }
    }

    abstract fun onImageUriReceived(uri: Uri)

    companion object {
        const val EXTRA_IMAGE_URI = "image_uri"
    }
}