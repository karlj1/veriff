package com.example.testapp.sdk

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection

internal class CapturePersonPhotoActivity : CameraActivity() {

    override fun onImageUriReceived(uri: Uri) {
        val inputImage = InputImage.fromFilePath(
            this,
            uri
        )

        FaceDetection.getClient()
            .process(inputImage)
            .addOnSuccessListener { faces ->
                Log.d(TAG, "Captured ${faces.size} faces")

                if (faces.size == 1) {
                    val intent = Intent()
                    intent.putExtra(EXTRA_IMAGE_URI, uri)

                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else if (faces.size == 0) {
                    val errorText = "No person detected. Please try again."

                    Log.e(TAG, errorText)

                    Toast.makeText(
                        this,
                        errorText,
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val errorText = "Photo must only have one person. Please try again."

                    Log.e(TAG, errorText)

                    Toast.makeText(
                        this,
                        errorText,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            .addOnFailureListener {
                val errorText = "Error during face detection. Please try again."

                Log.e(TAG, errorText, it)

                Toast.makeText(
                    this,
                    errorText,
                    Toast.LENGTH_LONG
                ).show()
            }
    }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}