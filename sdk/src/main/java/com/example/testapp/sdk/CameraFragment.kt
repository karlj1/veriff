package com.example.testapp.sdk

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.testapp.sdk.Constants.FILE_NAME
import com.example.testapp.sdk.Constants.RESULT_PERMISSIONS_DENIED
import com.example.testapp.sdk.databinding.FragmentSdkCameraBinding
import java.io.File
import java.util.concurrent.Executor

internal class CameraFragment(
    private val executor: Executor,
    private val imageCapture: ImageCapture,
    private val preview: Preview
) : Fragment() {

    private var _binding: FragmentSdkCameraBinding? = null
    private val binding get() = _binding!!

    private val cameraViewModel: CameraViewModel by activityViewModels()

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                startCamera()
            } else {
                requireActivity().setResult(RESULT_PERMISSIONS_DENIED)
                requireActivity().finish()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSdkCameraBinding.inflate(
            inflater,
            container,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.takePhoto.setOnClickListener {
            takePhoto()
            it.isEnabled = false
        }

        if (permissionsGranted()) {
            startCamera()
        } else {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun permissionsGranted(): Boolean {
        val permissionStatus = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        )

        return permissionStatus == PackageManager.PERMISSION_GRANTED
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider = cameraProviderFuture.get()

            // Preview
            preview.setSurfaceProvider(binding.viewFinder.surfaceProvider)

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this,
                    cameraSelector,
                    preview,
                    imageCapture
                )

            } catch (exc: Exception) {
                val errorText = "Error starting camera"
                Log.e(TAG, errorText, exc)

                Toast.makeText(
                    requireContext(),
                    errorText,
                    Toast.LENGTH_LONG
                ).show()
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }


    private fun takePhoto() {
        // Create a cache file for the image to be saved
        val file = File.createTempFile(
            FILE_NAME,
            null,
            requireActivity().cacheDir
        )

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(file)
            .build()

        imageCapture.takePicture(
            outputOptions,
            executor,
            object : ImageCapture.OnImageSavedCallback {

                @ExperimentalGetImage
                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    cameraViewModel.postUri(Uri.fromFile(file))
                }

                override fun onError(exception: ImageCaptureException) {
                    val errorText = "Error taking a picture. Please try again."
                    Log.e(TAG, errorText, exception)

                    Toast.makeText(
                        requireContext(),
                        errorText,
                        Toast.LENGTH_LONG
                    ).show()

                    binding.takePhoto.isEnabled = true
                }

            }
        )
    }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}