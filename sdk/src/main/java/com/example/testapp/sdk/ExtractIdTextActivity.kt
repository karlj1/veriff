package com.example.testapp.sdk

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.example.testapp.sdk.Constants.EXTRA_EXTRACTED_TEXT
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizerOptions

internal class ExtractIdTextActivity : CameraActivity() {

    override fun onImageUriReceived(uri: Uri) {
        val inputImage = InputImage.fromFilePath(
            this,
            uri
        )

        val recognizer =
            TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

        recognizer.process(inputImage)
            .addOnSuccessListener { visionText ->
                val text = visionText.text
                Log.d(TAG, text)

                if (text.isEmpty()) {
                    Toast.makeText(
                        this,
                        // These texts should be moved to strings.xml in the future
                        "No text extracted. Please try again.",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val intent = Intent()
                    intent.putExtra(EXTRA_EXTRACTED_TEXT, visionText.text)

                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
            .addOnFailureListener { e ->
                val errorText = "Error during text recognition. Please try again."

                Log.e(TAG, errorText, e)

                Toast.makeText(
                    this,
                    errorText,
                    Toast.LENGTH_LONG
                ).show()
            }
    }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}