package com.example.testapp.sdk

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MySdkTest {

    private lateinit var instrumentationContext: Context

    @Before
    fun setUp() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
    }

    @Test
    fun getExtractIdTextIntent() {
        val intent = MySdk.getExtractIdTextIntent(instrumentationContext)

        Assert.assertEquals(
            ExtractIdTextActivity::class.java.canonicalName,
            intent.component?.className
        )
    }

    @Test
    fun getExtractIdTextResult() {
        val text = "foo"
        val intent = Intent().putExtra(Constants.EXTRA_EXTRACTED_TEXT, text)

        Assert.assertEquals(
            text,
            MySdk.getExtractIdTextResult(intent)
        )
    }

    @Test
    fun getCapturePersonPhotoIntent() {
        val intent = MySdk.getCapturePersonPhotoIntent(instrumentationContext)

        Assert.assertEquals(
            CapturePersonPhotoActivity::class.java.canonicalName,
            intent.component?.className
        )
    }

    @Test
    fun getCapturePersonPhotoResult() {
        val uri = Uri.Builder().appendPath("foo").build()
        val intent = Intent().putExtra(CameraActivity.EXTRA_IMAGE_URI, uri)

        Assert.assertEquals(
            uri,
            MySdk.getCapturePersonPhotoResult(intent)
        )
    }
}