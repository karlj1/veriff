package com.example.testapp.sdk

import androidx.camera.core.ImageCapture
import androidx.camera.core.Preview
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.Executors

@RunWith(AndroidJUnit4::class)
class CameraFragmentTest {

    @Test
    fun testFragment() {
        val executor = Executors.newSingleThreadExecutor()
        val imageCapture = ImageCapture.Builder().build()
        val preview = Preview.Builder().build()

        launchFragmentInContainer {
            CameraFragment(
                executor,
                imageCapture,
                preview
            )
        }

        onView(withId(R.id.take_photo)).check(matches(isCompletelyDisplayed()))
        onView(withId(R.id.view_finder)).check(matches(isDisplayed()))
    }
}