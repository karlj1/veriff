package com.example.testapp.sdk

import android.net.Uri
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class CameraViewModelTest {

    private lateinit var sut: CameraViewModel

    @Before
    fun setUp() {
        sut = CameraViewModel()
    }

    @Test
    fun `Should emit correct values`() {
        val uri = Mockito.mock(Uri::class.java)

        sut.postUri(uri)

        Assert.assertEquals(uri, sut.uri.value)
    }
}