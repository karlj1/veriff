package com.example.testapp

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import com.example.testapp.sdk.MySdk
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.btn_detect_face
import kotlinx.android.synthetic.main.activity_main.btn_read_text
import kotlinx.android.synthetic.main.activity_main.image_face
import kotlinx.android.synthetic.main.activity_main.txt_result

class MainActivity : AppCompatActivity() {

    private val extractIdTextLauncher =
        registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                txt_result.text = MySdk.getExtractIdTextResult(result.data!!)
            } else if (result.resultCode == MySdk.RESULT_PERMISSIONS_DENIED) {
                Toast.makeText(
                    this,
                    "Permissions not granted",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    private val capturePersonPhotoLauncher =
        registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                Picasso.get()
                    .load(MySdk.getCapturePersonPhotoResult(result.data!!))
                    .into(image_face)
            } else if (result.resultCode == MySdk.RESULT_PERMISSIONS_DENIED) {
                Toast.makeText(
                    this,
                    "Permissions not granted",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_read_text.setOnClickListener {
            extractIdTextLauncher.launch(MySdk.getExtractIdTextIntent(this))
        }

        btn_detect_face.setOnClickListener {
            capturePersonPhotoLauncher.launch(MySdk.getCapturePersonPhotoIntent(this))
        }
    }
}
